# Web3 Dashboard Code Sample

This project uses Next.js 14 with Tailwind.css for styling. At a high level, the functionality is straightforward - fetching data and rendering into a data grid. But, the goal of this project is to demonstrate readable, composable, and scalable code, leveraging modern techniques and a clean design. The free [CoinCap API](https://docs.coincap.io/#intro) is used to provide data for demonstration purposes.

![Dashboard image](public/dashboard.png)

## Strategies

### Rendering

This project leverages React Server Components for top level pages, while using client components as needed deeper in the component tree.

### Data Fetching

This project uses [React Query](https://tanstack.com/query/latest) for data fetching. The main purpose for this is to leverage client side caching. This allows for faster component renders when switching between pages. It also replaces the need for a traditional "data store", as we can use the client side cache to reuse data across components. Our queries are stored in a separate [queries](src/queries/) directory as reusable hooks, which handle loading states, caching, and revalidation.

### Data Grid

This project leverages [React Table](https://tanstack.com/table/latest) under the hood. As a headless tool, it allows for full UI customizability, while handling common tasks like sorting and filtering. To build a robust, reusable `<DataGrid />` component, we leverage a modern technique, known as "generic components". This allows the component to receive a generic type, increasing it's flexibility. The [data-grid.test.tsx](src/components/common/data-grid/data-grid.test.tsx) file demonstrates the `DataGrid` component being rendered with a generic type `TestRow`.

### Testing

This project leverages Jest with React Testing Library for basic UI testing. Only a few tests were added for demonstration purposes, though in a more robust setup, a full test suite with code coverage would be beneficial.

## Running the Project

### Local

Install dependencies:

```bash
npm install
```

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Testing

To run the tests, simply run:

```bash
npm test
```

### Building

To build for production, simply run:

```bash
npm run build
```
