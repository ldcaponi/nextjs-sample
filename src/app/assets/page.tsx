import AssetsSummary from "@/components/assets-summary";
import TopAssets from "@/components/top-assets";

export default function AssetsPage() {
  return (
    <div className="flex flex-col md:flex-row">
      <div className="w-full lg:w-2/3">
        <TopAssets />
      </div>

      <div className="w-full lg:w-1/3 lg:pl-10">
        <AssetsSummary />
      </div>
    </div>
  );
}
