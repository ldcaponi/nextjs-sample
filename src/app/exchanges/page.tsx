import ExchangesSummary from "@/components/exchanges-summary";
import TopExchanges from "@/components/top-exchanges";

export default function ExchangesPage() {
  return (
    <div className="flex flex-col md:flex-row">
      <div className="w-full lg:w-2/3">
        <TopExchanges />
      </div>

      <div className="w-full lg:w-1/3 lg:pl-10">
        <ExchangesSummary />
      </div>
    </div>
  );
}
