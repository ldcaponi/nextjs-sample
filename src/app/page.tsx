"use client";

import { useRouter } from "next/navigation";
import { useEffect } from "react";

export default function Home() {
  const router = useRouter();

  // Demo app has no need for root page
  // Redirect immediately to /assets
  useEffect(() => {
    router.push("/assets");
  }, [router]);

  return <main />;
}
