"use client";

import Link from "@/components/common/link";
import { TABS } from "@/constants";
import { usePathname } from "next/navigation";

export default function TabSelector() {
  const currentPath = usePathname();

  return (
    <div className="flex items-center h-16">
      {TABS.map((tab) => (
        <Link
          key={tab.href}
          href={tab.href}
          isActive={tab.href === currentPath}
        >
          {tab.name}
        </Link>
      ))}
    </div>
  );
}
