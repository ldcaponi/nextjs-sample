"use client";

import useTopAssets from "@/queries/useTopAssets";
import DataItem from "@/components/common/data-item";
import WidgetContainer from "@/components/common/widget-container";
import numeral from "numeral";

export default function AssetsSummary() {
  const { data } = useTopAssets();

  return (
    <WidgetContainer title="Assets Summary">
      {data && data.length > 0 && (
        <>
          <DataItem
            label="Highest price"
            value={numeral(data[0].priceUsd).format("$0,0.00")}
          />
          <DataItem
            label="Lowest price"
            value={numeral(data[data.length - 1].priceUsd).format("$0,0.00")}
          />
        </>
      )}
    </WidgetContainer>
  );
}
