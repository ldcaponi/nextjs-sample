import { twMerge } from "tailwind-merge";

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: React.ReactNode;
  variant?: "primary" | "secondary" | "ghost";
  iconRight?: React.ReactNode;
  isActive?: boolean;
}

export default function Button({
  children,
  variant,
  className,
  iconRight,
  isActive,
  ...otherProps
}: Props) {
  let colorClass: string = "";
  let bgColorClass: string = "";

  if (!variant || variant === "primary") {
    colorClass = "text-gray-900";
    bgColorClass = "bg-gray-200";
  }

  if (variant === "secondary") {
    colorClass = "text-gray-200";
    bgColorClass = "bg-gray-800";
  }

  if (variant === "ghost") {
    bgColorClass = "bg-transparent";
    colorClass = isActive ? "text-gray-100" : "text-gray-400";
  }

  const commonClasses = "font-medium text-sm px-2.5 py-1.5 rounded";

  const finalClass = twMerge(
    commonClasses,
    colorClass,
    bgColorClass,
    className
  );
  return (
    <button className={finalClass} {...otherProps}>
      <span className="flex items-center gap-1">
        {children}
        {iconRight && iconRight}
      </span>
    </button>
  );
}
