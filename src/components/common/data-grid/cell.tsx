export function BasicCell({ children }: { children: React.ReactNode }) {
  return <div className="text-white text-sm font-normal py-3">{children}</div>;
}

// TODO: add href capability
export function LinkCell({
  children,
  href,
}: {
  children: React.ReactNode;
  href?: string;
}) {
  return (
    <a
      target="_blank"
      href={href}
      className="text-blue-400 text-sm font-normal py-3"
    >
      {children}
    </a>
  );
}
