export default function ColumnHeader({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="text-white opacity-70 text-xs text-left font-normal py-3">
      {children}
    </div>
  );
}
