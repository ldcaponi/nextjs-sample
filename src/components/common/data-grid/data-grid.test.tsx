import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

import DataGrid from ".";
import { ColumnDef, createColumnHelper } from "@tanstack/react-table";

type TestRow = {
  name: string;
  age: number;
};

const columnHelper = createColumnHelper<TestRow>();

const columns = [
  columnHelper.accessor("name", {
    id: "name",
    header: () => <div>Name</div>,
    cell: (info) => <div>{info.getValue()}</div>,
    sortingFn: "alphanumeric",
  }),
  columnHelper.accessor("age", {
    id: "age",
    header: () => <div>Age</div>,
    cell: (info) => <div>{info.getValue()}</div>,
    sortingFn: "alphanumeric",
  }),
] as ColumnDef<TestRow>[];

const dummyData: TestRow[] = [
  {
    name: "Alice",
    age: 10,
  },
  {
    name: "Bob",
    age: 20,
  },
];

describe("Navbar", () => {
  it("renders a grid", () => {
    render(
      <DataGrid<TestRow>
        title="Test Title"
        columns={columns}
        data={dummyData}
      />
    );

    expect(screen.getByText("Alice")).toBeInTheDocument();
    expect(screen.getByText("Bob")).toBeInTheDocument();
    expect(screen.getByText("Test Title")).toBeInTheDocument();
    expect(screen.getByText("10")).toBeInTheDocument();
    expect(screen.getByText("20")).toBeInTheDocument();
  });
});
