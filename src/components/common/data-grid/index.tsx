import {
  ColumnDef,
  SortingState,
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import TextInput from "../text-input";
import { useState } from "react";
import SearchIcon from "../icons/search";

type Props<T> = {
  title: string;
  data: T[];
  columns: ColumnDef<T>[];
};

export default function DataGrid<RowType>({
  title,
  data,
  columns,
}: Props<RowType>) {
  const [filterText, setFilterText] = useState<string>("");
  const [sorting, setSorting] = useState<SortingState>([]);

  const table = useReactTable({
    data: data || [],
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    state: {
      globalFilter: filterText,
      sorting,
    },
    onGlobalFilterChange: setFilterText,
    onSortingChange: setSorting,
    enableGlobalFilter: true,
    enableSorting: true,
  });

  return (
    <div className="w-full">
      <div className="flex h-20 items-center">
        <div className="text-gray-200 text-base font-bold">{title}</div>
        <div className="ml-auto">
          <TextInput
            value={table.getState().globalFilter}
            onChange={(e) => table.setGlobalFilter(e.target.value)}
            placeholder="Filter"
            leftIcon={<SearchIcon />}
          />
        </div>
      </div>

      <div className="bg-gray-800 rounded-md p-6">
        <table>
          <thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => (
                  <th
                    className="cursor-pointer select-none"
                    onClick={header.column.getToggleSortingHandler()}
                    style={{
                      width: header.getSize(),
                    }}
                    key={header.id}
                  >
                    {header.isPlaceholder
                      ? null
                      : flexRender(
                          header.column.columnDef.header,
                          header.getContext()
                        )}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody>
            {table.getRowModel().rows.map((row) => (
              <tr key={row.id}>
                {row.getVisibleCells().map((cell) => (
                  <td
                    style={{
                      width: cell.column.getSize(),
                    }}
                    key={cell.id}
                  >
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
        {table.getRowModel().rows.length === 0 && (
          <div className="text-white opacity-70 text-xs text-center my-20">
            No data to show
          </div>
        )}
      </div>
    </div>
  );
}
