import DataIcon from "../icons/data";

type Props = {
  label: string;
  value?: string;
};

export default function DataItem({ label, value }: Props) {
  return (
    <div className="text-gray-300 my-8">
      <div>
        <DataIcon />
      </div>
      <div className="text-xs">{label}</div>
      <div className="text-gray-200">{value}</div>
    </div>
  );
}
