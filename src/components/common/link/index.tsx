import NextLink, { LinkProps } from "next/link";
import { twMerge } from "tailwind-merge";

interface Props extends LinkProps {
  children: React.ReactNode;
  className?: string;
  isActive?: boolean;
}
// Simple wrapper component for next/link - adding some classes
export default function Link({
  children,
  className,
  isActive,
  ...otherProps
}: Props) {
  return (
    <NextLink
      className={twMerge(
        "font-medium text-sm text-gray-400 px-2.5 py-1.5",
        isActive && "text-gray-100",
        className
      )}
      {...otherProps}
    >
      {children}
    </NextLink>
  );
}
