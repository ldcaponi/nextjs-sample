type Props = {
  children: React.ReactNode;
};

export default function MaxWidthWrapper({ children }: Props) {
  return <div className="container mx-auto">{children}</div>;
}
