import Button from "../button";
import ArrowDropDownIcon from "../icons/arrow-drop-down";
import SearchIcon from "../icons/search";
import Link from "../link";
import TextInput from "../text-input";

export default function Navbar() {
  return (
    <div className="flex w-full h-16 items-center">
      <div className="text-white text-base font-bold">Web3 Dashboard</div>
      <div className="ml-auto mr-16">
        <Link href="/">Home</Link>
        <Link href="/">GitHub</Link>
        <Link href="/">Careers</Link>
        <Link href="/">Contact</Link>
      </div>

      <div className="mr-8">
        <TextInput leftIcon={<SearchIcon />} placeholder="Search..." />
      </div>

      <div className="ml-4">
        <Button className="mr-4" iconRight={<ArrowDropDownIcon />}>
          Sign Up
        </Button>
        <Button variant="secondary" iconRight={<ArrowDropDownIcon />}>
          Connect Wallet
        </Button>
      </div>
    </div>
  );
}
