import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Navbar from ".";

describe("Navbar", () => {
  it("renders a logo", () => {
    render(<Navbar />);
    const logo = screen.getByText("Web3 Dashboard");
    expect(logo).toBeInTheDocument();
  });

  it("renders links", () => {
    render(<Navbar />);
    const homeLink = screen.getByText("Home");
    const githubLink = screen.getByText("GitHub");
    const careersLink = screen.getByText("Careers");
    const contactLink = screen.getByText("Contact");

    expect(homeLink).toBeInTheDocument();
    expect(githubLink).toBeInTheDocument();
    expect(careersLink).toBeInTheDocument();
    expect(contactLink).toBeInTheDocument();
  });

  it("renders sign up and connect wallet button", () => {
    render(<Navbar />);
    const signUp = screen.getByText("Sign Up");
    const connectWallet = screen.getByText("Connect Wallet");

    expect(signUp).toBeInTheDocument();
    expect(connectWallet).toBeInTheDocument();
  });
});
