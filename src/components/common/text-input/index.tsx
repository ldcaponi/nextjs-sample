import { twMerge } from "tailwind-merge";

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  leftIcon?: React.ReactNode;
}

export default function TextInput({
  className,
  leftIcon,
  ...otherProps
}: Props) {
  return (
    <div className="relative flex items-center text-gray-100">
      <span className="absolute left-1">{leftIcon}</span>
      <input
        className={twMerge(
          "border border-gray-600 placeholder-gray-600 text-gray-400 text-sm bg-transparent rounded-md px-4 py-1",
          className,
          leftIcon && "pl-6"
        )}
        {...otherProps}
      />
    </div>
  );
}
