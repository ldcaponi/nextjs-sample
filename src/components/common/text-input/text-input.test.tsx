import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import TextInput from ".";

describe("TextInput", () => {
  it("renders a placeholder", () => {
    render(<TextInput placeholder="Search..." />);
    const input = screen.getByPlaceholderText("Search...");
    expect(input).toBeInTheDocument();
  });
});
