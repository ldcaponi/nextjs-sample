type Props = {
  title: string;
  children: React.ReactNode;
};

export default function WidgetContainer({ title, children }: Props) {
  return (
    <div className="border border-gray-800 px-8 pt-12 pb-16 rounded-md w-full">
      <div className="text-gray-100 text-base font-bold my-4">{title}</div>
      {children}
    </div>
  );
}
