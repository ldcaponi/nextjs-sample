"use client";

import DataItem from "@/components/common/data-item";
import WidgetContainer from "@/components/common/widget-container";
import numeral from "numeral";
import useTopExchanges from "@/queries/useTopExchanges";

export default function ExchangesSummary() {
  const { data } = useTopExchanges();

  return (
    <WidgetContainer title="Exchanges Summary">
      {data && data.length > 0 && (
        <>
          <DataItem
            label="Highest volume"
            value={numeral(data[0].volumeUsd).format("$0,0.00")}
          />
          <DataItem
            label="Lowest volume"
            value={numeral(data[data.length - 1].volumeUsd).format("$0,0.00")}
          />
        </>
      )}
    </WidgetContainer>
  );
}
