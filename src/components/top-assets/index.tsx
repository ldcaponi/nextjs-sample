"use client";

import DataGrid from "../common/data-grid";
import topAssetsColumns from "./top-assets-column-config";
import useTopAssets, { Asset } from "@/queries/useTopAssets";

export default function TopAssets() {
  const { data } = useTopAssets();

  return (
    <DataGrid<Asset>
      columns={topAssetsColumns}
      title={`Top Assets`}
      data={data || []}
    />
  );
}
