import numeral from "numeral";
import { ColumnDef, createColumnHelper } from "@tanstack/react-table";
import ColumnHeader from "../common/data-grid/column-header";
import { BasicCell, LinkCell } from "../common/data-grid/cell";
import { Asset } from "@/queries/useTopAssets";

const columnHelper = createColumnHelper<Asset>();

const topAssetsColumns = [
  columnHelper.accessor("rank", {
    id: "rank",
    header: () => <ColumnHeader>#</ColumnHeader>,
    cell: (info) => <BasicCell>{info.getValue()}</BasicCell>,
    size: 50,
    sortingFn: "alphanumeric",
  }),
  columnHelper.accessor("name", {
    id: "name",
    header: () => <ColumnHeader>Name</ColumnHeader>,
    cell: (info) => <BasicCell>{info.getValue()}</BasicCell>,
    size: 200,
    sortingFn: "auto",
  }),
  columnHelper.accessor("symbol", {
    id: "symbol",
    header: () => <ColumnHeader>Symbol</ColumnHeader>,
    cell: (info) => <BasicCell>{info.getValue()}</BasicCell>,
    size: 200,
    sortingFn: "auto",
  }),
  columnHelper.accessor("priceUsd", {
    id: "priceUsd",
    header: () => <ColumnHeader>Price USD</ColumnHeader>,
    cell: (info) => (
      <BasicCell>{numeral(info.getValue()).format("$0,0.00")}</BasicCell>
    ),
    size: 200,
    sortingFn: "auto",
  }),
] as ColumnDef<Asset>[];

export default topAssetsColumns;
