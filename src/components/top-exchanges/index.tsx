"use client";

import DataGrid from "../common/data-grid";
import topExchangesColumns from "./top-exchanges-column-config";
import useTopExchanges, { Exchange } from "@/queries/useTopExchanges";

export default function TopExchanges() {
  const { data } = useTopExchanges();

  return (
    <DataGrid<Exchange>
      columns={topExchangesColumns}
      title={`Top Assets`}
      data={data || []}
    />
  );
}
