import numeral from "numeral";
import { ColumnDef, createColumnHelper } from "@tanstack/react-table";
import ColumnHeader from "../common/data-grid/column-header";
import { BasicCell, LinkCell } from "../common/data-grid/cell";
import { Asset } from "@/queries/useTopAssets";
import { Exchange } from "@/queries/useTopExchanges";

const columnHelper = createColumnHelper<Exchange>();

const topExchangesColumns = [
  columnHelper.accessor("rank", {
    id: "rank",
    header: () => <ColumnHeader>#</ColumnHeader>,
    cell: (info) => <BasicCell>{info.getValue()}</BasicCell>,
    size: 50,
    sortingFn: "alphanumeric",
  }),
  columnHelper.accessor("name", {
    id: "name",
    header: () => <ColumnHeader>Name</ColumnHeader>,
    cell: (info) => (
      <LinkCell href={info.row.original.exchangeUrl}>
        {info.getValue()}
      </LinkCell>
    ),
    size: 200,
    sortingFn: "auto",
  }),
  columnHelper.accessor("percentTotalVolume", {
    id: "percentTotalVolume",
    header: () => <ColumnHeader>% Total Volume</ColumnHeader>,
    cell: (info) => (
      <BasicCell>{`${numeral(info.getValue()).format("0.000")}%`}</BasicCell>
    ),
    size: 200,
    sortingFn: "auto",
  }),
  columnHelper.accessor("volumeUsd", {
    id: "volumeUsd",
    header: () => <ColumnHeader>Volume USD</ColumnHeader>,
    cell: (info) => (
      <BasicCell>{`${numeral(info.getValue()).format("$0,0.00")}`}</BasicCell>
    ),
    size: 200,
    sortingFn: "auto",
  }),
] as ColumnDef<Exchange>[];

export default topExchangesColumns;
