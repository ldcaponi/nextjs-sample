type TabItem = {
  name: string;
  href: string;
};
export const TABS: TabItem[] = [
  { name: "Assets", href: "/assets" },
  { name: "Exchanges", href: "/exchanges" },
];
