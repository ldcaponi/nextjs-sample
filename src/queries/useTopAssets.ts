import { useQuery } from "@tanstack/react-query";

export type Asset = {
  id: string;
  name: string;
  rank: string;
  symbol: string;
  supply: string;
  maxSupply: string;
  marketCapUsd: string;
  volumeUsd24Hr: string;
  priceUsd: string;
  vwap24Hr: string;
};

export default function useTopAssets() {
  return useQuery({
    queryKey: ["top-assets"],
    queryFn: async () => {
      const res = await fetch(`https://api.coincap.io/v2/assets?limit=10`);
      return (await res.json()).data as Asset[];
    },
  });
}
