import { useQuery } from "@tanstack/react-query";

export type Exchange = {
  id: string;
  name: string;
  rank: string;
  percentTotalVolume: string;
  volumeUsd: string;
  tradingPairs: string;
  socket: boolean;
  exchangeUrl: string;
  updated: number;
};

export default function useTopExchanges() {
  return useQuery({
    queryKey: ["top-exchanges"],
    queryFn: async () => {
      const res = await fetch(`https://api.coincap.io/v2/exchanges?limit=10`);
      return (await res.json()).data as Exchange[];
    },
  });
}
